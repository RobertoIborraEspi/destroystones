using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterfaceAwake : MonoBehaviour
{
    public AudioClip MenuSound;
    public GameObject Camera;
    // Start is called before the first frame update
    void Start()
    {
        AudioSource.PlayClipAtPoint(MenuSound, Camera.transform.position);
        GameManager.CreatedStones = 0;
        GameManager.DestroyedStones = 0;
        GameManager.TotalDestroyedStones = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Click()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Instructions()
    {
        SceneManager.LoadScene("Instructions");
    }
    public void Quit()
    {
        Application.Quit();
    }
}
