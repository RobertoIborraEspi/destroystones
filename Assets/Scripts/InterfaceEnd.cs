using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceEnd : MonoBehaviour
{
    public Text textThrow;
    public Text textDestroy;
    public Text textPoints;
    public AudioClip GameResult;
    public GameObject Camera;

    // Start is called before the first frame update
    void Start()
    {
        AudioSource.PlayClipAtPoint(GameResult, Camera.transform.position);
        if (GameManager.CreatedStones == 26) textThrow.text = "Number of asteroids: 25";
        else textThrow.text = "Number of asteroids: " + GameManager.CreatedStones;
        textDestroy.text = "Destroyed: " + GameManager.DestroyedStones;
        textPoints.text = "Total Points: " + (GameManager.TotalDestroyedStones * 100);
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Click()
    {
        SceneManager.LoadScene("Awake");
    }
}
