using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterfaceInstructions : MonoBehaviour
{
    public AudioClip MenuSound;
    public GameObject Camera;
    // Start is called before the first frame update
    void Start()
    {
        AudioSource.PlayClipAtPoint(MenuSound, Camera.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Click()
    {
        SceneManager.LoadScene("Awake");
    }
}
