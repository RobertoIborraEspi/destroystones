using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceStone : MonoBehaviour
{
    public Text textThrow;
    public Text textDestroy;
    public Text textPoints;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textThrow.text = "Number of asteroids: " + GameManager.CreatedStones;
        textDestroy.text = "Destroyed: " + GameManager.DestroyedStones;
        textPoints.text = "Total Points: " + (GameManager.TotalDestroyedStones * 100);
    }
}
