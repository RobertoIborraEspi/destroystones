using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceCompleteLevel : MonoBehaviour
{
    public AudioClip MenuSound;
    public AudioClip GameResult;
    public Text textThrow;
    public Text textDestroy;
    public Text textPoints;
    public GameObject Camera;
    public string next;
    // Start is called before the first frame update
    void Start()
    {
        AudioSource.PlayClipAtPoint(GameResult, Camera.transform.position);
        textThrow.text = "Number of asteroids: " + (GameManager.CreatedStones - 1);
        textDestroy.text = "Destroyed: " + GameManager.DestroyedStones;
        textPoints.text = "Total Points: " + (GameManager.TotalDestroyedStones * 100);
        AudioSource.PlayClipAtPoint(MenuSound, Camera.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Click()
    {
        GameManager.CreatedStones = 0;
        GameManager.DestroyedStones = 0;
        SceneManager.LoadScene(next);
    }
}
